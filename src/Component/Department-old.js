import React, { useState, useEffect } from 'react';

import { Table, TableCell, TableRow, TableBody,  Typography } from "@material-ui/core";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';




const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 6,
};


const Department = () => {
    const [user, setUser] = useState([]);
    const [open, setOpen] = useState(false);


     const [dept, setDept] = useState({
        name:"",

    });
    const { name } = dept;
    const onInputChange = e => {
        setDept({  [e.target.name]: e.target.value });
        {console.log(dept)}
      };


    useEffect(() => {
        getUser();
       

    },[]);



    //To get the value
    const getUser = () => {
        fetch('https://officeemployee.herokuapp.com/api/department')
            .then(response => response.json())
            .then(data => setUser(data.data));
          addUser();

    }


    //To add the department
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    const addUser = () => {

        fetch('https://officeemployee.herokuapp.com/api/department',{
            method :"POST",

        }
       

        )
            .then(response => response.json())
            .then(data => setDept(data))
            alert('Data Inserted');
           
    }

    //Delete the user  
    /* const DeleteUser =(_id)=>{
        {console.log(_id)}
       
        fetch(`https://officeemployee.herokuapp.com/api/department/${_id}`,{
            method:"DELETE",
            Header:"content-type:application/json"
            
        })
        .then(response=>response.json())
        .then(resp=>{
            console.log(resp);
            // getUser();
        })
        
    } */

    //Add the department




    //View the user department
    const viewUser = (name) => {
        { alert(name) }


    }
    return (
        <div className="container ">
            <div className='row'>
                <div className='col-md-6'>
                    <h2>Department list</h2>
                </div>
                <div className="col-md-6">
                    <input type="text" />
                    <Button  variant="contained" color="primary">Reset</Button>
                    <Button onClick={handleOpen} variant="contained" color="success">Insert</Button>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                        <Typography>Add the Department</Typography>
                        <TextField  color="secondary" focused placeholder='Enter Department Name'name="name" value={name} onChange={e => onInputChange(e)}/> 
                        <Button  variant="contained" color="primary"  onClick={() => addUser()} >Save</Button>



                        </Box>
                    </Modal>
                </div>

            </div>
            <Table >

                <TableBody >
                    <TableRow  >
                        <TableCell >Slno</TableCell>
                        <TableCell >Id</TableCell>
                        <TableCell >Department Name</TableCell>
                        <TableCell >Action</TableCell>
                    </TableRow>
                </TableBody>


                <TableBody >
                    {
                        user.map((item, i) => (
                            <TableRow key={i}>
                                <TableCell  >
                                    {i + 1}
                                </TableCell>
                                <TableCell >
                                    {item._id}
                                </TableCell>
                                <TableCell >
                                    {item.name}
                                </TableCell>
                                <Button color="primary" variant="contained" onClick={() => viewUser(item.name)} >View</Button>

                                <Button color="error" variant="outlined" >Delete</Button>
                            </TableRow>

                        )


                        )
                    }

                </TableBody>
            </Table>

        </div>
    )
}

export default Department;
