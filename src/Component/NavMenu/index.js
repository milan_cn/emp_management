import React from 'react';
import { Link,useHistory } from "react-router-dom";


const NavMenu = () => {
    let user = JSON.parse(localStorage.getItem("userToken"))
    const History=useHistory();
    const LogOut=()=>{
       

        localStorage.removeItem("userToken");
        window.location.reload();
        //History.push("/login")
    }

    return (
        <div>
           <nav>
          <ul>
           <li>
              <Link to="/department">Department</Link>
            </li>
            <li>
              <Link to="/employee">Employee</Link>
            </li>
            
            <li>
              <Link to = "/login" onClick={LogOut}>Logout</Link>
            </li>
          </ul>
        </nav>

            
        </div>
    )
}

export default NavMenu;
/* import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import MenuItem from '@mui/material/MenuItem';


 const ResponsiveAppBar = () => {

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
            >
         
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              
              
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
             
                <MenuItem >
                  <Typography textAlign="center" color="black">page</Typography>
                  <Typography textAlign="center">page</Typography>  
                  <Typography textAlign="center">page</Typography>
                </MenuItem>
            
            </Menu>
          </Box>
          
      

          
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
 */
