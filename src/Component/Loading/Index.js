import React from 'react';
// import disconnect from "disconnect.png"

const RoutLoading = ({error}) => {
    if(error)
    return (
        <div>
         <div className="route-loading error">
        <div className="container">
          <div className="row">
            <div className="col">
              <h3>
                {/* <img src="public/disconnect.png" alt="disconnect" /> */}
                Oh no! Please check your network connection...
              </h3>
            </div>
          </div>
        </div>
      </div>
            
        </div>
    )
};

export default RoutLoading;
