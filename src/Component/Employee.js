import React, { useState, useEffect } from 'react';
import { Table, TableCell, TableRow, TableBody, Typography } from "@material-ui/core";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import RootApi from '../RootApi';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
     p: 6,
};


const Employee = () => {
    const [upEmployee, setUpEmployee] = useState([]);
    const [newEditValue, setNewEditValue] = useState({})
    const [updateName, setUpdateName] = useState({
        name:"",
        department:""

    });
    const [open, setOpen] = useState(false);
   const [openUpdate, setOpenUpdate] = useState(false)
    const [addEmployee, setAddEmployee] = useState({
         name: "",
        department:"",
    });
   const { name,department } = addEmployee;
    const onInputChange = e => {
        setAddEmployee({ [e.target.name]: e.target.value });
        { console.log(addEmployee) }
    };

    useEffect(() => {
        getUser()

    }, [])



    //************To get the value**********/
   

    
    const getEmpWithDept = () => {
        RootApi.get("/employee?populate=department").then((empDept) => {
            console.log('getEmpWithDept->', empDept.data.data);
            setUpEmployee(empDept?.data?.data)
         } )
        }
    const getUser = () => {
        RootApi.get('/employee').then((response) => {
            console.log('res->', response);
            setAllEmployee(response.data.data)
        }).catch((err) => {
            console.log(err);
         })

    }
    // const getEmployeeByid = (_id) => {
    //     RootApi.get(`/employee/${_id}?populate=department`).then((response) => {
    //         console.log('getuser->', response);
    //         setAllEmployee(response.data.data)
    //     }).catch((err) => {
    //         console.log(err);
    //     })

    // }

    //***************To update the user ****************//
    // const getDepartment = () => {
    //     RootApi.get('/department').then((response) => {
    //         console.log('department->', response);
    //         setDepartment(response.data.data)
    //     }).catch((err) => {
    //         console.log(err);
    //     })

    // }

    const handleEditOpen = (item) =>{                         

        console.log("item",item)
        const newData={
            _id:item._id,
            name:item.name,
            department:item.department._id
        }
        setNewEditValue(newData)
        setOpenUpdate(true)


    }
        
    const handleUpdateClose = () => setOpenUpdate(false);


    const updateUser=(_id)=>{
        console.log(_id)
     
       
     
        RootApi.put(`./employee/${_id}`,{
            
              updateName,
            dept

        })


             }

    //*******To add the department***************//
   // *********************************************//

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
   
    const addUser=(_id,name,department)=>{
        RootApi.post('/employee',{
            id:"_id",  
           name:`${name}`,
           department:`${department}` 
           
    
           
        }).then((response)=>{
            console.log("res====",response)
            setAddEmployee(response)
        }).catch((err)=>{
            console.log(err)

        })
        getEmpWithDept();
    }
    //**************Delete the user****************//

    const deleteUser =(_id) => {
        alert(_id)
        RootApi.delete(`./employee/${_id}`).then((response)=>{
            console.log("employeeupdate=>",response)
            getEmpWithDept();

        })
            .catch((err) => {
                console.log(err)
            })
    }
    
    //**************View the user department************//

    const viewUser = (name) => {
        { alert(name) }


    }
    return (
        <div className="container ">
            <div className='row'>
                <div className='departmentLeftSection'>
                    <h2>Employee list</h2>
                </div>
                <div className="departmentRightsection">
                
                    <input type="text" placeholder='search department' />
                    <div className='btn_design'>
                    <Button variant="contained" color="primary">Reset</Button>
                    <Button onClick={handleOpen} variant="contained" color="success">Insert</Button>
                    </div>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography>Add the Department</Typography>
                           
                            <TextField color="secondary" focused placeholder='Enter Department Name' name="name" value={name} onChange={e => onInputChange(e)} />
                            <TextField color="secondary" focused placeholder='Enter employee Name' name="department" value={department} onChange={e => onInputChange(e)} />
                            <Button variant="contained" color="primary" onClick={() => addUser()} >Save </Button>
                        </Box>
                    </Modal>



                    <Modal
                        open={openUpdate}
                        onClose={handleUpdateClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        {/* <Box sx={style}>
                            <Typography>Update the employee list</Typography>
                            <form >
                            <label>Employee Name</label>
                            <input type='text' name="name" id="name" value={newEditValue.name} onChange={(e)=>setNewEditValue({...newEditValue,name:e.target.value}) } />
                            <label>Department Name</label>
                            <select onChange={(e)=>setNewEditValue({...newEditValue, department:e.target.value})}>
                            <option value="">Select Option</option>
                            {
                                department && department.map((data, i) => (
                                    <>
                                    <option value={data._id} key={i} selected={newEditValue.department === data._id} >{data.name}</option>

                                    </>

                                ))
                            }
                            </select>
                            <Button variant="contained" color="primary" onClick={() => updateUser()}>Save </Button>

                            </form>
                            <TextField color="secondary" focused placeholder='Employee Name' name="department" value={department} onChange={e => onTextFieldChange(e)} />
                            <TextField color="secondary" focused placeholder='department Name' name="name" value={name} onChange={e => onTextFieldChange(e)}  />
                            <Button variant="contained" color="primary" onClick={() => updateUser(name)}>Save </Button>
                        </Box> */}
                    </Modal>
                </div>

            </div>
            <Table >

                <TableBody >
                    <TableRow  >
                        <TableCell >Slno</TableCell>
                        <TableCell >Employee name</TableCell>

                        <TableCell >Department </TableCell>
                        <TableCell >Action</TableCell>
                    </TableRow>
                    
                </TableBody>
                <TableBody >
                    {upEmployee &&
                        upEmployee.map((item, i) => (
                            <TableRow key={i}>
                                <TableCell  >
                                    {i + 1}
                                </TableCell>
                                <TableCell >
                                    {item.name}
                                </TableCell>
                                <TableCell >
                                    {item.department.name}
                                </TableCell>
                                <Button color="primary" variant="contained" onClick={() => viewUser(item.name)} >View</Button>
                                <Button color="success" variant="contained" onClick={()=>handleEditOpen(item)}  >Update</Button>
                                <Button color="error" variant="contained" onClick={()=>deleteUser(item._id)} >Delete</Button>
                            </TableRow>

                        )


                        )
                    }

                </TableBody>
            </Table>

        </div>
    )
}
export default Employee;
