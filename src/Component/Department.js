import React, { useState, useEffect } from 'react';
import { Table, TableCell, TableRow, TableBody,  Typography } from "@material-ui/core";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button'
import Modal from '@mui/material/Modal'
import TextField from '@mui/material/TextField';
import RootApi from '../RootApi';
import "./dept.css"


const style = 
 {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 6
 }


const Department = ({data}) => 
{
    console.log('RootApi', RootApi);
    const [allDepartment, setAllDepartment] = useState([]);
    const [inputValue, setInputValue] = useState("");
    const [filteredData,setFilteredData] = useState(allDepartment);
    const [open, setOpen] = useState(false);
     const [dept, setDept] = useState({
    name:"",

    });
    const { name } = dept;
    const onInputChange = e => {
        setDept({  [e.target.name]: e.target.value });
        {console.log(dept)}
      };

   //Search the value
      const handleFilter=((e)=>{
          const searchWord = e.target.value;
          const newFilter = allDepartment .filter((value)=>{
             return value.name.toLowerCase().includes(searchWord.toLowerCase())
          });
          if(searchWord===""){
            setFilteredData([])  
          }else{
            setFilteredData(newFilter)

          }
          setInputValue(e.target.value);
          
      })


    useEffect(() => {
        getUser();
      
    
    },[]);



    //To get the value
    const getUser = () => {
        RootApi.get('/department').then((response) => {
            console.log('res->', response);
            setAllDepartment(response.data.data)
        }).catch((err) => {
            console.log(err);
        })

    }

    //To add the department
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
   
    const addUser=()=>{

        RootApi.post('/department',{
            id:"_id",
            name:`${name}`
           
        }).then((response)=>{
            console.log("res====",response)
            setDept(response)
            getUser();
        }).catch((err)=>{
            console.log(err)

        })  
        
    }
//Delete the user

const DeleteUser=(_id)=>{

    RootApi.delete(`/department/${_id}`).then((response)=>{
        console.log('deleteuser=>',response);
        getUser();

    })
    .catch((err)=>{
        console.log(err)
        
    })


}

    //View the user department
    const viewUser = (name) => {
        { alert(name) }
    }

    //Reset the value
    const resetInputField=()=>{
        setInputValue("");
    }
    const name="milan";
    const person={
        first:name
    };
    console.log(person);
    const sayHello=(fname)=>{
        console.log(`${fname}`);

    };
    sayHello('milan')
    return (
        <div className="container ">
            <div className='row'>
                <div className='departmentLeftSection'>
                    <h2>Department list</h2>
                </div>
                <div className="departmentRightsection">
                    <input type="text" name="input" value={inputValue}  onChange={handleFilter}/>
                    <div className='btn_design'>
                    <Button  variant="contained" color="primary" onClick={resetInputField} >Reset</Button>
                    <Button onClick={handleOpen} variant="contained" color="success">Insert</Button>
                    </div>
                    </div>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                        <Typography>Add the Department</Typography>
                        <TextField  color="secondary" focused placeholder='Enter Department Name'name="name" value={name} onChange={e => onInputChange(e)}/> 
                        <Button  variant="contained" color="primary"  onClick={() => addUser()} >Save</Button>
                        </Box>
                    </Modal>
               

            </div>
           
            <Table >

                <TableBody >
                    <TableRow  >
                        <TableCell >Slno</TableCell>
                        <TableCell >Id</TableCell>
                        <TableCell >Department Name</TableCell>
                        <TableCell >Action</TableCell>
                    </TableRow>
                </TableBody>


                <TableBody >
                    {
                        filteredData.length >0 ? filteredData.map((item, i) => (
                            <TableRow key={i}>
                                <TableCell  >
                                    {i + 1}
                                </TableCell>
                                <TableCell >
                                    {item._id}
                                </TableCell>
                                <TableCell >
                                    {item.name}
                                </TableCell>
                                <Button color="primary" variant="contained" onClick={() => viewUser(item.name)} >View</Button>
                                <Button color="error" variant="contained" onClick={() => DeleteUser(item._id)} >Delete</Button>
                            </TableRow>
                            
                            
                        )

):

                        allDepartment.map((item, i) => (
                            <TableRow key={i}>
                                <TableCell  >
                                    {i + 1}
                                </TableCell>
                                <TableCell >
                                    {item._id}
                                </TableCell>
                                <TableCell >
                                    {item.name}
                                </TableCell>
                                <Button color="primary" variant="contained" onClick={() => viewUser(item.name)} >View</Button>

                                <Button color="error" variant="outlined" onClick={() => DeleteUser(item._id)} >Delete</Button>
                            </TableRow>

                        )


                        )
                    }
                    
                </TableBody>
            </Table>

        </div>
    )
}

export default Department;
