import Loadable from 'react-loadable';
//import RoutLoading from '../Component/Loading/Index';


const Employee = Loadable({
    loader: () => import('../Component/Employee'),
    loading:'loading',
  });

  const Department = Loadable({
    loader: () => import('../Component/Department'),
    loading: 'loading',
  });  
  const Login = Loadable({
    loader: () => import('../Pages/Login'),
    loading: 'loading',
  });
  export{
      Employee,
      Department,
      Login
  }