import React, {useState, useEffect} from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import NavMenu from '../Component/NavMenu';
//import NavMenu from '../Component/NavMenu';
import ResponsiveAppBar from "../Component/NavMenu"
import {
      Employee,
      Department,
      Login
  } from "./Route";


const PrivateRoute = ({component: Component, auth, name, ...rest }) => (
    <Route
      {...rest}
      render={(props) => (auth === true ? <Component {...props} name={name} /> : <Redirect to="/login" />)}
    />
);
  
const PublicRoute = ({ component: Component, auth, ...rest }) => (
    <Route
      {...rest}
      render={(props) => (auth !== true ? <Component {...props} /> : <Redirect to="/" />)}
    />
);

 
 const AppRouter = () => {
      const [enableRoute, setEnableRoute] = useState(false);
      useEffect(() => {
        const userData = localStorage.getItem('userToken');
          if(userData !==null){
              setEnableRoute(true)
          }else{
            setEnableRoute(false)
          }
        
       },[]);

     return (
         <div>
         {
            enableRoute && <NavMenu/>
         }
                <Switch>
                    {/*Only for Public Route */}
                    {/* <PublicRoute exact auth={enableRoute}  path="/" component={Login}/> */}
                    <PublicRoute exact auth={enableRoute}  path="/login" component={Login}/>

                    {/*Only for Private Route */}
                    <PrivateRoute exact auth={enableRoute}  path="/department" component={Department}/>
                    <PrivateRoute exact auth={enableRoute}  path="/employee" component={Employee}/>

                    {/* condonation */}
                    {enableRoute ? (
                            <Redirect exact from="/" to="/department" />
                        ) : (
                            <Redirect from="/" to="/login" />
                        )}
                </Switch>
           
         </div>
     )
 }
 
 export default AppRouter;
 



  





