import axios from 'axios';
import axiosRetry from 'axios-retry';
import {apiHost} from './constants';


const RootApi = axios.create({
    baseURL: apiHost,
})

axiosRetry(RootApi, {retries: 3});

// Add a request interceptor
RootApi.interceptors.request.use((config) => {

    const userData = localStorage.getItem('userToken');
  
    config.headers.Authorization =  userData ? `Token ${JSON.parse(userData).token}` : '';
  
    return config;
  });
export { RootApi as default };