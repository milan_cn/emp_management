import React,{useState} from "react";
import "./Login.css"
import { useForm } from "react-hook-form";
import RootApi from '../RootApi';
import {ToastContainer,toast} from 'react-toastify';
import { Link} from "react-router-dom";

function Login() {
  const [loading, setLoading] = useState(false)
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm();
 

//To set the Token in Localstorage

 const userAuthenticationToken=(tokenDetails)=>{
  localStorage.setItem("userToken",JSON.stringify(tokenDetails))
 }


  const loginSubmit=(item)=>{
    setLoading(true)
    RootApi.post("/loginasadmin",item
     
    ).then((res)=>{
      console.log(res)
   if(res.status ===200){
    userAuthenticationToken(res.data.data);
    toast.success("Login Successful !", {
      position: toast.POSITION.TOP_LEFT
    }) 
    setTimeout(()=>{
      setLoading(false)
      window.location.reload()
    

    },3000)


   }
   else{
    alert("fal")

   }
     
    }).catch((err)=>{
      console.log(err)
    })
  }
  const onSubmit = (data) => {
    loginSubmit(data);
    reset();

   
    //alert(JSON.stringify(data));
  }; 


  return (
    <form onSubmit={handleSubmit(onSubmit)}>
    <ToastContainer autoClose={2000}/>
      <label>Email</label>
      <input
        {...register("email", {
          required: true,
          pattern: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i
        })} type="text" placeholder="Enter your Email"  
      />
      {errors?.email?.type === "required" && <p>This field is required</p>}
      
      {errors?.email?.type === "pattern" && (
        <p>Email is not proper format</p>
      )}
    
      <label>Password</label>
      <input {...register("password", {required:true ,minLength:"8" })} type="text" placeholder="Enter your password" />
      {errors?.email?.type === "required" && <p>This field is required</p>}
      {errors?.password?.type === "minLength" && (
        <p>Minium 8 characters in needed</p>
      )}
    
      <button disabled={loading} type="submit"><Link to="/login">{loading?"Loading......":"Log In"}</Link></button>

    </form>
  );
}

export default Login;
