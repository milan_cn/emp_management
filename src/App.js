
import './App.css';
/* import Department from './Component/Department';
import Employee from './Component/Employee';
 */
// import Login from './Pages/Login';
import AppRouter from './Router/Index';

function App() {
  return (
    <div className="App">
    <AppRouter/>
    </div>
  );
}

export default App;
